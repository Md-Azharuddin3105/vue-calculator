new Vue({
    el: '#App',
    data: {
        previousNumber: "",
        currentNumber: "",
        input: "",
        operator: ""
      },
    methods: {
        addToInput: function(val){
            this.input += val
        },
        clearInput: function(){
            this.input = ""
        },
        addDecimal: function(val){
            if(this.input.indexOf('.') === -1){
                this.input += val
            }
        },
        addZero: function(val){
            if(this.input !== ""){
                this.input += val
            }
        },
        divide: function(){
            this.previousNumber = this.input
            this.input = ""
            this.operator = "divide"
        },
        multiply: function(){
            this.previousNumber = this.input
            this.input = ""
            this.operator = "multiply"
        },
        subtract: function(){
            this.previousNumber = this.input
            this.input = ""
            this.operator = "subtract"
        },
        add: function(){
            this.previousNumber = this.input
            this.input = ""
            this.operator = "add"
        },
        evaluate: function(){
            this.currentNumber = this.input
            if(this.operator === "add"){
                this.input = parseFloat(this.previousNumber) + parseFloat(this.currentNumber)
            }
            else if (this.operator === "subtract"){
                this.input = parseFloat(this.previousNumber) - parseFloat(this.currentNumber)
            }
            else if (this.operator === "multiply"){
                this.input = parseFloat(this.previousNumber) * parseFloat(this.currentNumber)
            }
            else if (this.operator === "divide"){
                this.input = (parseFloat(this.previousNumber) / parseFloat(this.currentNumber)).toFixed(2)
            }
        }
    },
});